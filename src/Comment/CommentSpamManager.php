<?php

declare(strict_types=1);

namespace App\Comment;

use App\Entity\Comment;
use RuntimeException;

final class CommentSpamManager
{
    private CommentSpamCounterInterface $spamWordCounter;

    public function __construct(CommentSpamCounterInterface $spamWordCounter)
    {
        $this->spamWordCounter = $spamWordCounter;
    }

    public function validate(Comment $comment): void
    {
        $content = (string) $comment->getContent();
        $badWordsCount = $this->spamWordCounter->countSpamWords($content);
        if ($badWordsCount >= 2) {
            // We could throw a custom exception if needed
            throw new RuntimeException('Message detected as spam');
        }
    }
}
