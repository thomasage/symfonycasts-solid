<?php

declare(strict_types=1);

namespace App\Scoring;

use App\Entity\BigFootSighting;

final class DescriptionFactor implements ScoringFactorInterface
{
    public function score(BigFootSighting $sighting): int
    {
        $score = 0;
        $title = strtolower((string) $sighting->getDescription());

        if (false !== stripos($title, 'hairy')) {
            $score += 10;
        }

        if (false !== stripos($title, 'chased me')) {
            $score += 20;
        }

        if (false !== stripos($title, 'using an iPhone')) {
            $score -= 50;
        }

        return $score;
    }
}
