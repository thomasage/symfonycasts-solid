<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\BigFootSighting;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class BigfootSightingType extends AbstractType
{
    /**
     * @param FormBuilderInterface<FormBuilderInterface> $builder
     * @param array<mixed>                               $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('title')
            ->add('description')
            ->add('latitude')
            ->add('longitude')
            ->add(
                'images',
                TextareaType::class,
                [
                    'help' => 'Add the URL where the images live separated by commas',
                    'required' => false,
                ]
            );

        $builder->get('images')->addModelTransformer(
            new CallbackTransformer(
                function ($images) {
                    if (!$images) {
                        return '';
                    }

                    // transform the array to a string
                    return implode(', ', $images);
                },
                function ($images) {
                    if (!$images) {
                        return [];
                    }

                    // transform the string back to an array
                    return explode(', ', $images);
                }
            )
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => BigFootSighting::class,
            ]
        );
    }
}
