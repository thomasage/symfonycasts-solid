<?php

declare(strict_types=1);

namespace App\Service;

use App\Comment\CommentSpamCounterInterface;

final class RegexSpamWordHelper implements CommentSpamCounterInterface
{
    public function countSpamWords(string $content): int
    {
        return count($this->getMatchedSpamWords($content));
    }

    /**
     * @return string[]
     */
    private function getMatchedSpamWords(string $content): array
    {
        $badWordsOnComment = [];
        $regex = implode('|', $this->spamWords());
        preg_match_all("/$regex/i", $content, $badWordsOnComment);

        return $badWordsOnComment[0];
    }

    /**
     * @return string[]
     */
    private function spamWords(): array
    {
        return [
            'follow me',
            'twitter',
            'facebook',
            'earn money',
            'SymfonyCats',
        ];
    }
}
