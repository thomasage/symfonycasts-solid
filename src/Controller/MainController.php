<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\BigFootSightingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class MainController extends AbstractController
{
    #[Route('/', name: 'app_homepage')]
    public function homepage(
        BigFootSightingRepository $bigFootSightingRepository
    ): Response {
        $sightings = $bigFootSightingRepository->findLatest(25);

        return $this->render(
            'main/homepage.html.twig',
            [
                'sightings' => $sightings,
                'mostActiveSightings' => $sightings,
            ]
        );
    }

    #[Route('/about', name: 'app_about')]
    public function about(): Response
    {
        return $this->render('main/about.html.twig');
    }
}
