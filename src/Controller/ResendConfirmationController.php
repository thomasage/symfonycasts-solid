<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Service\ConfirmationEmailSender;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ResendConfirmationController extends AbstractController
{
    #[Route('/resend-confirmation', methods: ['POST'])]
    public function resend(
        ConfirmationEmailSender $confirmationEmailSender
    ): Response {
        $this->denyAccessUnlessGranted('ROLE_USER');
        /** @var User $user */
        $user = $this->getUser();

        $confirmationEmailSender->send($user);

        return new Response(null, 204);
    }
}
